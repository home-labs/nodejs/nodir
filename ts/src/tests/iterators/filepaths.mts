// import NodeUrl from 'node:url';

// absolute file directory
// console.log('\n')
// console.log('file that fired: ', url.fileURLToPath(import.meta.url))
// console.log('\n')


import NodePath from 'node:path';
import { Path } from '@cyberjs.on/nodir';

import {
    Directory,
    IteratorsProtocol
} from '../../index.mjs';


const projectRootDirectory = process.cwd();

Path.operatingSystemPathSeparator = NodePath.sep;

const pathname = Path.join(projectRootDirectory, 'es');

console.log('\n')
console.log('Files except of .json types from', pathname)
console.log('\n')

const fileOptions = {
    extensions: [
        'json'
        , '*.map'
    ]
    // ,
    // extensions4Ignore: [
    //     'json'
    // ]
};

console.log('fileOptions', fileOptions)

const directory = new Directory(pathname);

const listOfFilePath = directory.resolveListOfFilePath();

console.log('All path of files:', listOfFilePath.map(
    path => path.toString()
), '\n');

const iterator = new IteratorsProtocol.FilePathIteratorProtocol(pathname, fileOptions);

console.log('Catched filenames: [');
for (let path of iterator) {
    console.log('  ', path?.basename);
}
console.log(']')
