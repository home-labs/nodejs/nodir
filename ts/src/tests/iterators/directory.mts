import {
    TreeIteratorDepthStrategy
    // , TreeIteratorBreadthStrategy
} from '@cyberjs.on/graphite';

import {
    Directory,
    DirectoryData,
    IteratorsProtocol
} from '../../index.mjs';


const projectRootDirectory = process.cwd();

const exceptionsData: DirectoryData = {
    paths: [
        '.git',
        'es',
        'node_modules'
    ]
};

const directorynames: string[] = [];

const iteratorProtocol = new IteratorsProtocol
    .DirectoryIteratorProtocol(new Directory(projectRootDirectory), exceptionsData);

// const iterator = new TreeIteratorBreadthStrategy(iteratorProtocol);
const iterator = new TreeIteratorDepthStrategy(iteratorProtocol);


for (let directory of iterator) {
    directorynames.push(directory!.pathname);
}

console.log('root directory:', projectRootDirectory, '\n');

console.log(directorynames);
