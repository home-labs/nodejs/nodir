import NodeFs from 'node:fs';
import NodePath from 'node:path';

import {
    Path,
    Helpers as NodirHelper
} from '@cyberjs.on/nodir';

import { FileOptions } from './index.mjs';


export class Directory {

    #path: Path;

    #pathname: string;

    #childrenPaths!: Path[];

    constructor(realPathname: string) {

        const _self = Directory;

        /**
         *
         * @warning DON'T MOVE this line
         */
        Path.operatingSystemPathSeparator = NodePath.sep;

        this.#path = _self.#resolvePath(realPathname);

        this.#pathname = this.#path.toString();
    }

    resolveListOfFilePath(fileOptions?: FileOptions) {

        const _self = Directory;

        let path: Path;

        return NodeFs.readdirSync(
            this.#pathname,
            {
                withFileTypes: true
            }
        )
            .filter(
                directoryEntry => {
                    path = new Path(directoryEntry.name);

                    /**
                     *
                     * @todo adicionar suporte a padrão com wildcard (abstrair lógica de Copfy canCopyingAccordingExtension e helper File de Nodir)
                     */
                    return directoryEntry.isFile()
                        && (!fileOptions
                            || (!fileOptions.extensions4Ignore?.length
                                && !fileOptions.extensions?.length
                            )
                            || (path.extension
                                && (

                                    /**
                                     *
                                     * a prioridade é a proibição
                                     */
                                    (
                                        fileOptions.extensions4Ignore?.length
                                        // && !fileOptions!.extensions4Ignore?.includes(path.extension)
                                        && !_self.#matchAccordingExtension(path.toString(), fileOptions!.extensions4Ignore)
                                    )
                                    || (
                                        !fileOptions.extensions4Ignore?.length
                                        && fileOptions.extensions?.length
                                        // && fileOptions!.extensions?.includes(path.extension)
                                        && _self.#matchAccordingExtension(path.toString(), fileOptions!.extensions)
                                    )

                                    /**
                                     *
                                     * a prioridade é a permissão
                                     */
                                    // (fileOptions.extensions?.length
                                    //     && fileOptions.extensions?.includes(path.extension)
                                    // )
                                    // || (
                                    //     !fileOptions.extensions?.length
                                    //     && fileOptions.extensions4Ignore?.length
                                    //     && !fileOptions!.extensions4Ignore?.includes(path.extension)
                                    // )
                                )
                            )
                        );
                }
            )
            .map(
                fileEntry => {
                    return new Path(Path.join(this.#pathname, fileEntry.name));
                }
            );

    }

    getChildren(exceptions: string[] = []) {

        return NodeFs.readdirSync(
            this.#pathname,
            {
                withFileTypes: true
            }
        )
            .filter(
                directoryEntry =>
                    directoryEntry.isDirectory()
                    && !exceptions.includes(
                        new Path(Path.join(this.#pathname, directoryEntry.name)).toString()
                    )
            )
            .map(
                directoryEntry =>
                    new Directory(Path.join(this.#pathname, directoryEntry.name))
            );

    }

    attachNew(pathname: string, verbose = false) {

        const _self = Directory;

        return _self.create(Path.join(this.#pathname, pathname), verbose);
    }

    static isDirectory(realPathname: string) {

        const path = new Path(realPathname);

        if (NodeFs.existsSync(path.toString())
            && NodeFs.statSync(path.toString()).isDirectory()
        ) {
            return true;
        }

        return false;
    }

    static create(
        pathname: string,
        verbose = false
    ) {

        const folders2Iterate = new Path(pathname).toString().split(Path.separator);

        const folders = [...folders2Iterate];

        const foldersname2Create = [];

        let originalAbsolutePathname = '';

        let mountedDirectoryname = '';

        let mountedAbsolutePathname = '';

        if (!pathname) {
            return new Directory('');
        }

        if (!this.isDirectory(pathname)) {

            for (let i = folders2Iterate.length - 2; i >= 0; i--) {

                foldersname2Create.unshift(folders.pop());
                originalAbsolutePathname = folders.join(Path.separator);

                if (NodeFs.existsSync(originalAbsolutePathname)) {
                    break;
                }
            }

            if (foldersname2Create.length) {

                for (let foldername of foldersname2Create) {
                    mountedDirectoryname = Path.join(mountedDirectoryname, foldername!);
                    mountedAbsolutePathname = Path.join(originalAbsolutePathname, mountedDirectoryname);
                    NodeFs.mkdirSync(mountedAbsolutePathname);
                }
            }

            if (verbose) {
                console.log(`\nThe pathname '${mountedDirectoryname}' has been attached at '${originalAbsolutePathname}'`);
            }

            return new Directory(mountedAbsolutePathname);
        }

        return new Directory(pathname);
    }

    static #resolvePath(pathname: string) {

        const path = new Path(pathname);

        if (NodeFs.existsSync(path.toString())) {

            if (this.isDirectory(path.toString())) {
                return path;
            }

            return new Path(path.basename || '');
        }

        throw new Error(`The informed path ${path.toString()} doesn't exist.`);
    }

    static #matchAccordingExtension = (
        filename: string | null,
        extensionPatterns: string[] = []
    ) => {

        const extension: string | null = new Path(filename || '').extension;

        let extensionsWithWildcard: string[];

        console.log('filename', filename)
        console.log('extension', extension)
        if (filename && extension) {

            extensionsWithWildcard = NodirHelper.File
                .collectWildCardExtensions(extensionPatterns);

            if (
                (extensionsWithWildcard.length
                    && NodirHelper.File
                        .extensionMatches2Wildcard(extension, extensionsWithWildcard)
                )
                || extensionPatterns.includes(extension)
            ) {
                return true;
            }
        }

        return false;
    };

    get childrenPaths() {
        return this.#childrenPaths;
    }

    get pathname() {
        return this.#pathname;
    }

    get path() {
        return this.#path;
    }

}
