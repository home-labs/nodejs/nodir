import { QueueIterator } from '@cyberjs.on/design-protocools/iterator';

import { Path } from '@cyberjs.on/nodir';

import {
    FileOptions,
    Directory
} from '../index.mjs';


export class FilePathIteratorProtocol extends QueueIterator<Path> {

    constructor(
        pathname: string,
        fileOptions?: FileOptions
    ) {
        super((new Directory(pathname)).resolveListOfFilePath(fileOptions));
    }

}
