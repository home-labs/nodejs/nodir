import { AbstractTreeIteratorProtocol } from '@cyberjs.on/graphite';

import {
    Directory,
    DirectoryData
} from '../index.mjs';
import { Path } from '@cyberjs.on/nodir';


export class DirectoryIteratorProtocol extends AbstractTreeIteratorProtocol<Directory> {

    #childDirectories: Directory[];

    #exceptions: string[];

    constructor(rootDirectory: Directory, exceptionsData?: DirectoryData) {
        super([rootDirectory]);

        this.#childDirectories = [];

        this.#exceptions = this.#filterPaths(rootDirectory.pathname, exceptionsData);
    }

    isLeafNode(currentDirectory: Directory): boolean {

        this.#childDirectories = currentDirectory.getChildren(this.#exceptions);

        if (this.#childDirectories.length) {
            return false;
        }

        return true;
    }

    resolveElements4NextIteration(): Directory[] {
        return this.#childDirectories;
    }

    #filterPaths(rootPathname: string, exceptionsData?: DirectoryData) {

        const _self = Directory;

        const mounted: string[] = [];

        let currentPathname: string;

        if (!exceptionsData) {
            exceptionsData = {} as DirectoryData;
        }

        if (!exceptionsData!.paths) {
            exceptionsData!.paths = [];
        }

        for (let pathname of exceptionsData.paths) {

            currentPathname = Path.join(rootPathname, pathname);

            if (_self.isDirectory(currentPathname)) {
                mounted.push(currentPathname);
            }
        }

        return mounted;
    }

}
